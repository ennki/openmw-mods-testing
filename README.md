# OpenMW Mods Testing

This is an unstable playground for testing out pybuilds that are not yet complete enough to go in the [OpenMW Mods repo](https://gitlab.com/portmod/openmw-mods).

Information in this repo may be misleading or even completely innaccurate. Usage of this repo as a portmod repository is discouraged as it is highly unstable.
